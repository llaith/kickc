// Test initializing array using KickAssembler

// Sinus table
byte SINTAB[256] = kickasm {{
    .fill 256, 128 + 128*sin(i*2*PI/256)
}};

byte* SCREEN = 0x400;

void main() {
    SCREEN[0] = SINTAB[0];

}